<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Model\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = Siswa::all();
        return $this->success($siswa, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nis' => 'unique:siswa,nis',
            'nama' => 'required|string',
            'gender' => 'required|in:laki-laki,perempuan',
            'email' => 'required|email|unique:siswa,email',
            'nama_ortu' => 'string',
            'alamat' => 'string',
            'phone_number' => 'min:9|max:15'
        ]);

        if($validator->fails()){
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $siswa = new Siswa();
        $siswa->nis = $request->nis;
        $siswa->nama = $request->nama;
        $siswa->gender = $request->gender;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tgl_lahir = $request->tgl_lahir;
        $siswa->email = $request->email;
        $siswa->nama_ortu = $request->nama_ortu;
        $siswa->alamat = $request->alamat;
        $siswa->phone_number = $request->phone_number;
        $siswa->kelas_id = $request->kelas_id;
        $saved = $siswa->save();
        if($saved){
            return $this->success($siswa, 201);
        } else {
            return $this->failedResponse('User gagal ditambahkan!', 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        return $this->success($siswa, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        $validator = Validator::make($request->all(), [
            'nis' => 'unique:siswa,nis',
            'nama' => 'required|string',
            'gender' => 'required|in:laki-laki,perempuan',
            'email' => 'required|email',
            'nama_ortu' => 'string',
            'alamat' => 'string',
            'phone_number' => 'min:9|max:15'
        ]);

        if($validator->fails()){
            $msg = $validator->errors();

            return $this->failedResponse($msg, 422);
        }

        $siswa->nis = $request->nis;
        $siswa->nama = $request->nama;
        $siswa->gender = $request->gender;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tgl_lahir = $request->tgl_lahir;
        $siswa->email = $request->email;
        $siswa->nama_ortu = $request->nama_ortu;
        $siswa->alamat = $request->alamat;
        $siswa->phone_number = $request->phone_number;
        $siswa->kelas_id = $request->kelas_id;
        $saved = $siswa->save();
        if($saved){
            return $this->success($siswa, 201);
        } else {
            return $this->failedResponse('User gagal diupdate!', 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa)
    {
        $deletedData = $siswa->delete();
        if($deletedData){
            return $this->success(null, 200);
        } else {
            return $this->failedResponse('Siswa gagal dihapus!', 500);
        }
    }

    private function success($data,$statusCode,$message='success')
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
            'status_code' => $statusCode,
            'auth' => Auth::user()
            ],$statusCode);
    }

    private function failedResponse($message,$statusCode)
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode,
            'auth' => Auth::user()
            ],$statusCode);
    }
}
